﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GMap.NET.WindowsForms;
using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms.Markers;
using System.Diagnostics; 

namespace GmapEn.Demo
{
    public partial class MainForm : Form
    {  
        public MainForm()
        {
            InitializeComponent(); 
            try
            {
                System.Net.IPHostEntry e = System.Net.Dns.GetHostEntry("ditu.google.cn");
            }
            catch
            {
                ucMap1.Manager.Mode = AccessMode.CacheOnly;
                MessageBox.Show("No internet connection avaible, going to CacheOnly mode.", "GMap.NET Demo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            ucMap1.CacheLocation = Environment.CurrentDirectory + "\\GMapCache\\"; //缓存位置
            ucMap1.MapProvider = GMapProviders.GoogleChinaMap; //.GoogleChinaMap; //google china 地图
            
            ucMap1.MinZoom = 3;  //最小比例
            ucMap1.MaxZoom = 24; //最大比例
            ucMap1.Zoom = 10;     //当前比例
            ucMap1.ShowCenter = false; //不显示中心十字点
            ucMap1.DragButton = System.Windows.Forms.MouseButtons.Right; //左键拖拽地图
            ucMap1.Position = new PointLatLng(31.481963, 104.212054); //地图中心 
            
              
        }    
         
        private void button1_Click(object sender, EventArgs e)
        {
            PointLatLng pt = new PointLatLng((double)numLat1.Value, (double)numLong1.Value);
            ucMap1.AddPosition( true,pt.Lat, pt.Lng);
             
        } 

        private void btnSetView_Click(object sender, EventArgs e)
        { 
            PointLatLng pt = new PointLatLng((double)numLat1.Value, (double)numLong1.Value);
            ucMap1.Position = pt; 
        }

        private void btnDirectFound_Click(object sender, EventArgs e)
        {
            float df = (float)numDf.Value;
            float elevation = 80.0f; 
            ucMap1.DrawDFLine(df, elevation);
        }

        private void btnCrossLocation_Click(object sender, EventArgs e)
        {
            PointLatLng pt = new PointLatLng((double)numLat1.Value, (double)numLong1.Value);
            ucMap1.AddPosition(false, pt.Lat, pt.Lng);
        }

        private void btnPostureLay_Click(object sender, EventArgs e)
        {
            List<PointLatLng> lst = new List<PointLatLng>();
            for (int i = 0; i < 100; i++)
			{
                   PointLatLng latlng= new PointLatLng();
                latlng.Lng = ucMap1.Position.Lng + i * 0.006;
                latlng.Lat = ucMap1.Position.Lat + i * 0.006;
                lst.Add(latlng);
            }
            ucMap1.AddPoints(lst);
        }

        private void btnNoDrag_Click(object sender, EventArgs e)
        {
            ucMap1.DragButton = System.Windows.Forms.MouseButtons.None;
        }
    }
}
