﻿namespace GmapEn.Demo
{
    partial class UCMap
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.ucEye1 = new GmapEn.Demo.UCEye();
            this.SuspendLayout();
            // 
            // ucEye1
            // 
            this.ucEye1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ucEye1.Bearing = 0F;
            this.ucEye1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucEye1.CanDragMap = true;
            this.ucEye1.EmptyTileColor = System.Drawing.Color.Navy;
            this.ucEye1.GrayScaleMode = false;
            this.ucEye1.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.ucEye1.LevelsKeepInMemmory = 5;
            this.ucEye1.Location = new System.Drawing.Point(0, 295);
            this.ucEye1.MarkersEnabled = true;
            this.ucEye1.MaxZoom = 1;
            this.ucEye1.MinZoom = 1;
            this.ucEye1.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.ucEye1.Name = "ucEye1";
            this.ucEye1.NegativeMode = false;
            this.ucEye1.PolygonsEnabled = true;
            this.ucEye1.RetryLoadTile = 0;
            this.ucEye1.RoutesEnabled = true;
            this.ucEye1.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.ucEye1.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.ucEye1.ShowTileGridLines = false;
            this.ucEye1.Size = new System.Drawing.Size(200, 124);
            this.ucEye1.TabIndex = 0;
            this.ucEye1.Zoom = 1D;
            // 
            // UCMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ucEye1);
            this.Name = "UCMap";
            this.Size = new System.Drawing.Size(573, 419);
            this.ResumeLayout(false);

        }

        #endregion

        private UCEye ucEye1;
    }
}
