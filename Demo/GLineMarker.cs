﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMap.NET;
using System.Drawing;

namespace GmapEn.Demo
{
     
    /// <summary>
    /// 线标记
    /// </summary>
    public class GLineMarker :GMap.NET.WindowsForms.GMapMarker
    {
        Pen m_Pen;
         Color m_LineColor;
        public Color LineColor
        {
            get { return m_LineColor; }
            set 
            {
                m_LineColor = value;
                m_Pen.Color = value;
            }
        }
        float m_fDirectFind;

        /// <summary>
        /// 绘制方向
        /// </summary>
        public float FDirectFind
        {
            get { return m_fDirectFind; }
            set { m_fDirectFind = value; }
        }

        public GLineMarker(PointLatLng ptLatLng, float fDirectFind)
            : base(ptLatLng)
        {
            m_LineColor = Color.Red;
            m_Pen = new Pen(m_LineColor, 2.0f);
            Offset = new System.Drawing.Point(0, 0);
            m_fDirectFind = fDirectFind;
            Size = new Size(100, 100);
        }
        public override void Dispose()
        {
            base.Dispose();
            m_Pen.Dispose();
        }
        public override void OnRender(System.Drawing.Graphics g)
        {
            if (!float.IsNaN(m_fDirectFind))
            {
                int nR = 2000;

                double dAng = m_fDirectFind * Math.PI / 180.0f;
                int nEndPtX = LocalPosition.X + (int)(nR * Math.Sin(dAng));
                int nEndPtY = LocalPosition.Y - (int)(nR * Math.Cos(dAng));

                g.DrawLine(m_Pen, LocalPosition.X, LocalPosition.Y, nEndPtX, nEndPtY);

            }
        }
    }
}
