﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GMap.NET.WindowsForms;
using GMap.NET.MapProviders;
using GMap.NET;
using GMap.NET.WindowsForms.Markers;
using System.Drawing.Imaging;
using System.Diagnostics;

namespace GmapEn.Demo
{
    public partial class UCMap : GMapControl　
    {
        float m_fDirectFind = float.NaN;
        float m_fElevation = float.NaN;

        double m_dLatVal = double.NaN;
        double m_dLonVal = double.NaN;
        //放置marker的图层
        private GMapOverlay m_MapOverlay;

        //放置态势的图层
        private GMapOverlay m_PostureLay;
         
        private GMapPolygon m_LinePolygon;
        Bitmap m_BmpLocation;
        Bitmap m_BmpPosture;
        SolidBrush m_LineBrush;
        /// <summary>
        ///  方向线是否更改
        /// </summary>
        bool m_bDFEidted = false; 
        bool m_bDfLineEnter = false;

        public UCMap()
        {
            InitializeComponent();
            m_MapOverlay = new GMapOverlay("Pointlay");
            m_PostureLay = new GMapOverlay("Posturelay");
            
            Overlays.Add(m_MapOverlay);
            Overlays.Add(m_PostureLay);
             
            
            Bitmap fingerBMP = new Bitmap("finger.png");
            Bitmap plnBMP = new Bitmap("Plane.png");
            Size szMk = new Size(12, 12);
            m_BmpLocation = new Bitmap(fingerBMP, szMk);
            m_BmpPosture = new Bitmap(plnBMP, szMk);
            fingerBMP.Dispose();
            plnBMP.Dispose();
            m_LineBrush = new SolidBrush(Color.Red);

            this.PolygonsEnabled = true;
            this.OnPolygonEnter += new PolygonEnter(UCMap_OnPolygonEnter);
            this.OnPolygonLeave += new PolygonLeave(UCMap_OnPolygonLeave);
            this.OnPolygonClick += new PolygonClick(UCMap_OnPolygonClick);
             
            this.OnMapZoomChanged += new MapZoomChanged(UCMap_OnMapZoomChanged);
            this.OnPositionChanged += new PositionChanged(UCMap_OnPositionChanged);
            
            ucEye1.Manager.Mode = AccessMode.ServerAndCache;
            ucEye1.CacheLocation = Environment.CurrentDirectory + "\\GMapCache\\"; //缓存位置
            ucEye1.MapProvider = GMapProviders.GoogleChinaMap; //.GoogleChinaMap; //google china 地图

            ucEye1.ShowCenter = true; //不显示中心十字点 
            ucEye1.Position = new PointLatLng(30, 80); //地图中心 
            ucEye1.OnPositionChanged += new PositionChanged(ucEye1_OnPositionChanged);
        }
        bool m_bChangeCalled = false;
        void ucEye1_OnPositionChanged(PointLatLng point)
        {
            if (m_bChangeCalled == false)
            {
                m_bChangeCalled = true;
                Position = point; 
            }
            m_bChangeCalled = false;
        }

        void UCMap_OnPositionChanged(PointLatLng point)
        {
            if (m_bChangeCalled == false)
            {
                m_bChangeCalled = true;
                ucEye1.Position = point;
            }
            SetEyeRect();
            m_bChangeCalled = false;
        }
        /// <summary>
        /// 设置鹰眼区域
        /// </summary>
        private void SetEyeRect()
        {
            PointLatLng p1 = FromLocalToLatLng(0, 0);
            PointLatLng p2 = FromLocalToLatLng(0, this.Height);
            PointLatLng p3 = FromLocalToLatLng(this.Right, this.Height);
            PointLatLng p4 = FromLocalToLatLng(this.Right, 0);
            List<PointLatLng> tmp = new List<PointLatLng>();
            tmp.Add(p1);
            tmp.Add(p2);
            tmp.Add(p3);
            tmp.Add(p4);
            ucEye1.SetRect(tmp);
        }
        
        void UCMap_OnMapZoomChanged()
        { 
            PointLatLng pt = new PointLatLng(m_dLatVal, m_dLonVal);
            AddLine(pt);
            SetEyeRect();
        }

        void UCMap_OnPolygonClick(GMapPolygon item, MouseEventArgs e)
        {
            // 收不到click事件！
            //m_bDFEidted = !m_bDFEidted;
        }
        
        void UCMap_OnPolygonLeave(GMapPolygon item)
        {
            if (m_LinePolygon == item)
            {
                m_LinePolygon.Stroke.Color = Color.Red;
                m_LineBrush.Color = Color.Red;
                m_bDfLineEnter = false;
            }
        }

        void UCMap_OnPolygonEnter(GMapPolygon item)
        {
            if (m_LinePolygon == item)
            {
                m_LinePolygon.Stroke.Color = Color.LightSkyBlue;
                m_LineBrush.Color = Color.LightSkyBlue;
                m_bDfLineEnter = true;
            }
        } 
     
        protected override void OnPaintOverlays(Graphics g)
        {
            base.OnPaintOverlays(g); 
            if (m_ptLatlng != null)
            {
                foreach (var item in m_ptLatlng)
                {
                    GPoint pt = FromLatLngToLocal(item);
                    g.DrawImage(m_BmpPosture, new Point((int)pt.X, (int)pt.Y));
                }
            }
        }
        List<PointLatLng> m_ptLatlng;
        public void AddPoints(List<PointLatLng> pts)
        {
            m_ptLatlng = pts;
            base.Refresh();
        }
        /// <summary> 
        /// 设置显示位置
        /// </summary>
        /// <param name="bBmp">  是否加载图标显示</param>
        /// <param name="dLatVal"></param>
        /// <param name="dLonVal"></param>
        public void AddPosition(bool bBmp, double dLatVal, double dLonVal)
        {
            m_dLatVal = dLatVal;
            m_dLonVal = dLonVal;
            PointLatLng pt = new PointLatLng((double)m_dLatVal, (double)m_dLonVal);
            AddLine(pt);

            GMapMarker marker = null;
            if (bBmp == false)
            { 
                marker = new GMarkerCross(pt);
            }
            else
            {
                marker = new GMarkerGoogle(pt, m_BmpLocation);
            }

            marker.ToolTipText = "";
            m_MapOverlay.Markers.Add(marker);

            base.Refresh();

        }
        private void AddLine(PointLatLng pt)
        {
            if (!float.IsNaN(m_fDirectFind) &&
                !double.IsNaN(pt.Lat) &&
                !double.IsNaN(pt.Lng))
            {
                List<PointLatLng> tmp = new List<PointLatLng>();


                double dAng = m_fDirectFind * Math.PI / 180.0f;
                GPoint ptLocal = FromLatLngToLocal(pt);

                int nLWidth = 3;

                // 使用通用的矩形绘制策略！ 
                GPoint ptLocal1 = new GPoint(); 
                ptLocal1.X = (long)(ptLocal.X + Math.Cos(dAng) * nLWidth);  
                ptLocal1.Y = (long)(ptLocal.Y + Math.Sin(dAng) * nLWidth);  
                                                                            


                int nXDist = (int)ptLocal.X - this.Width;
                int nR = 1000;
                int nEndPtX = ((int)ptLocal.X + (int)(nR * Math.Sin(dAng)));
                int nEndPtY = ((int)ptLocal.Y - (int)(nR * Math.Cos(dAng)));

                int nEndPtX1 = ((int)ptLocal1.X + (int)(nR * Math.Sin(dAng)));
                int nEndPtY1 = ((int)ptLocal1.Y - (int)(nR * Math.Cos(dAng)));
                PointLatLng pt4 = FromLocalToLatLng(nEndPtX, nEndPtY);
                PointLatLng pt2 = FromLocalToLatLng((int)ptLocal1.X, (int)ptLocal1.Y);
                PointLatLng pt3 = FromLocalToLatLng((int)nEndPtX1, (int)nEndPtY1);
                tmp.Add(pt);
                tmp.Add(pt2);
                tmp.Add(pt3);
                tmp.Add(pt4);

                if (m_LinePolygon != null)
                {
                    m_MapOverlay.Polygons.Remove(m_LinePolygon);
                    m_LinePolygon.Dispose();
                }
                m_LinePolygon = new GMapPolygon(tmp, "DFLine");
                m_LinePolygon.IsHitTestVisible = true; 

                m_LinePolygon.Stroke.Width = 1;
                m_LinePolygon.Stroke.Color = Color.Red;
                m_LinePolygon.Fill = m_LineBrush;
                m_MapOverlay.Polygons.Add(m_LinePolygon); 
            } 
        }
        /// <summary>
        /// 绘制线
        /// </summary>
        /// <param name="fDirectFind"></param>
        /// <param name="fElevation"></param>
        public void DrawDFLine(float fDirectFind, float fElevation)
        {
            m_fDirectFind = fDirectFind;
            m_fElevation = fElevation;
            PointLatLng pt = new PointLatLng(m_dLatVal, m_dLonVal);
            AddLine(pt);
            base.Refresh();
        }
        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
            if (m_LinePolygon != null && m_bDfLineEnter == true)
            {
                m_bDFEidted = !m_bDFEidted;
            }
        }
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (m_bDFEidted &&
                 !double.IsNaN(m_dLatVal)
                && !double.IsNaN(m_dLonVal)
                && !float.IsNaN(m_fDirectFind))
            {
                PointLatLng pt = new PointLatLng(m_dLatVal, m_dLonVal);
                PointLatLng ptMouse = FromLocalToLatLng(e.X, e.Y);
                GPoint ptOri = FromLatLngToLocal(pt);
                m_fDirectFind = (float)((Math.Atan((double)(e.X - ptOri.X) / (double)(ptOri.Y - e.Y))) * 180.0f / Math.PI);
                if (e.X >= ptOri.X && e.Y > ptOri.Y)
                {
                    m_fDirectFind += 180.0f;
                }
                else if (e.X < ptOri.X && e.Y > ptOri.Y)
                {
                    m_fDirectFind += 180.0f;
                }
                if (m_fDirectFind < 0)
                {
                    m_fDirectFind += 360.0f;
                }
                AddLine(pt);
                base.Refresh();
                Debug.WriteLine("m_fDirectFind={0}", m_fDirectFind);
            }
        }
    }
}
