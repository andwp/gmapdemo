﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace GmapEn.Demo
{
    public partial class FrmDialog : Form 
    { 
        public FrmDialog()
        {
            InitializeComponent(); 
            m_Timer  = new System.Threading.Timer(InvokeTest, null, 0, 100);
        }
        private void InvokeTest(object p)
        {

        }
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            m_Timer.Dispose();
        }
        private EventHandler m_Notify;
        public event EventHandler OnNotify
        {
            add { m_Notify += value; }
            remove { m_Notify -= value; }
        }
        private System.Threading.Timer m_Timer;
        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            this.DialogResult = System.Windows.Forms.DialogResult.OK; 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
