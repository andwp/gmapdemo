﻿namespace GmapEn.Demo
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.labTime = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labLocation = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.numDf = new System.Windows.Forms.NumericUpDown();
            this.btnDirectFound = new System.Windows.Forms.Button();
            this.numLong1 = new System.Windows.Forms.NumericUpDown();
            this.numLat1 = new System.Windows.Forms.NumericUpDown();
            this.btnSetView = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnCrossLocation = new System.Windows.Forms.Button();
            this.ucMap1 = new GmapEn.Demo.UCMap();
            this.btnPostureLay = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLong1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLat1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "当前时间：";
            // 
            // labTime
            // 
            this.labTime.AutoSize = true;
            this.labTime.Location = new System.Drawing.Point(86, 10);
            this.labTime.Name = "labTime";
            this.labTime.Size = new System.Drawing.Size(17, 12);
            this.labTime.TabIndex = 2;
            this.labTime.Text = "无";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(251, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "位置：";
            // 
            // labLocation
            // 
            this.labLocation.AutoSize = true;
            this.labLocation.Location = new System.Drawing.Point(299, 11);
            this.labLocation.Name = "labLocation";
            this.labLocation.Size = new System.Drawing.Size(17, 12);
            this.labLocation.TabIndex = 4;
            this.labLocation.Text = "无";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ucMap1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(113, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(712, 338);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "显示";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.labTime);
            this.panel1.Controls.Add(this.labLocation);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 338);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(825, 34);
            this.panel1.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(825, 338);
            this.panel2.TabIndex = 7;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnPostureLay);
            this.panel3.Controls.Add(this.numDf);
            this.panel3.Controls.Add(this.btnDirectFound);
            this.panel3.Controls.Add(this.numLong1);
            this.panel3.Controls.Add(this.numLat1);
            this.panel3.Controls.Add(this.btnSetView);
            this.panel3.Controls.Add(this.btnCrossLocation);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(113, 338);
            this.panel3.TabIndex = 6;
            // 
            // numDf
            // 
            this.numDf.DecimalPlaces = 2;
            this.numDf.Location = new System.Drawing.Point(17, 17);
            this.numDf.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numDf.Name = "numDf";
            this.numDf.Size = new System.Drawing.Size(81, 21);
            this.numDf.TabIndex = 1;
            this.numDf.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // btnDirectFound
            // 
            this.btnDirectFound.Location = new System.Drawing.Point(17, 44);
            this.btnDirectFound.Name = "btnDirectFound";
            this.btnDirectFound.Size = new System.Drawing.Size(75, 23);
            this.btnDirectFound.TabIndex = 1;
            this.btnDirectFound.Text = "方向";
            this.btnDirectFound.UseVisualStyleBackColor = true;
            this.btnDirectFound.Click += new System.EventHandler(this.btnDirectFound_Click);
            // 
            // numLong1
            // 
            this.numLong1.DecimalPlaces = 6;
            this.numLong1.Location = new System.Drawing.Point(17, 177);
            this.numLong1.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numLong1.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.numLong1.Name = "numLong1";
            this.numLong1.Size = new System.Drawing.Size(81, 21);
            this.numLong1.TabIndex = 2;
            this.numLong1.Value = new decimal(new int[] {
            104012311,
            0,
            0,
            393216});
            // 
            // numLat1
            // 
            this.numLat1.DecimalPlaces = 6;
            this.numLat1.Location = new System.Drawing.Point(17, 136);
            this.numLat1.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.numLat1.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.numLat1.Name = "numLat1";
            this.numLat1.Size = new System.Drawing.Size(81, 21);
            this.numLat1.TabIndex = 2;
            this.numLat1.Value = new decimal(new int[] {
            30381921,
            0,
            0,
            393216});
            // 
            // btnSetView
            // 
            this.btnSetView.Location = new System.Drawing.Point(17, 259);
            this.btnSetView.Name = "btnSetView";
            this.btnSetView.Size = new System.Drawing.Size(75, 23);
            this.btnSetView.TabIndex = 0;
            this.btnSetView.Text = "设置显示";
            this.btnSetView.UseVisualStyleBackColor = true;
            this.btnSetView.Click += new System.EventHandler(this.btnSetView_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(17, 204);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "设置位置";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnCrossLocation
            // 
            this.btnCrossLocation.Location = new System.Drawing.Point(17, 309);
            this.btnCrossLocation.Name = "btnCrossLocation";
            this.btnCrossLocation.Size = new System.Drawing.Size(75, 23);
            this.btnCrossLocation.TabIndex = 0;
            this.btnCrossLocation.Text = "设置位置2";
            this.btnCrossLocation.UseVisualStyleBackColor = true;
            this.btnCrossLocation.Click += new System.EventHandler(this.btnCrossLocation_Click);
            // 
            // ucMap1
            // 
            this.ucMap1.Bearing = 0F;
            this.ucMap1.CanDragMap = true;
            this.ucMap1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucMap1.EmptyTileColor = System.Drawing.Color.Navy;
            this.ucMap1.GrayScaleMode = false;
            this.ucMap1.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.ucMap1.LevelsKeepInMemmory = 5;
            this.ucMap1.Location = new System.Drawing.Point(3, 17);
            this.ucMap1.MarkersEnabled = true;
            this.ucMap1.MaxZoom = 2;
            this.ucMap1.MinZoom = 2;
            this.ucMap1.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.ucMap1.Name = "ucMap1";
            this.ucMap1.NegativeMode = false;
            this.ucMap1.PolygonsEnabled = true;
            this.ucMap1.RetryLoadTile = 0;
            this.ucMap1.RoutesEnabled = true;
            this.ucMap1.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.ucMap1.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.ucMap1.ShowTileGridLines = false;
            this.ucMap1.Size = new System.Drawing.Size(706, 318);
            this.ucMap1.TabIndex = 0;
            this.ucMap1.Zoom = 0D;
            // 
            // btnPostureLay
            // 
            this.btnPostureLay.Location = new System.Drawing.Point(17, 96);
            this.btnPostureLay.Name = "btnPostureLay";
            this.btnPostureLay.Size = new System.Drawing.Size(75, 23);
            this.btnPostureLay.TabIndex = 3;
            this.btnPostureLay.Text = "态势";
            this.btnPostureLay.UseVisualStyleBackColor = true;
            this.btnPostureLay.Click += new System.EventHandler(this.btnPostureLay_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 372);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numDf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLong1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLat1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private UCMap ucMap1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labLocation;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NumericUpDown numLong1;
        private System.Windows.Forms.NumericUpDown numLat1;
        private System.Windows.Forms.Button btnSetView;
        private System.Windows.Forms.Button btnDirectFound;
        private System.Windows.Forms.NumericUpDown numDf;
        private System.Windows.Forms.Button btnCrossLocation;
        private System.Windows.Forms.Button btnPostureLay;
    }
}