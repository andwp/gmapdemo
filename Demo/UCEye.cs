﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GMap.NET.WindowsForms;
using GMap.NET;

namespace GmapEn.Demo
{
    public partial class UCEye : GMapControl
    {
        private GMapPolygon m_EyeRect;
        //放置GMapPolygon的图层
        private GMapOverlay m_MapOverlay;
        public UCEye()
        {
            InitializeComponent();
            this.MaxZoom = 1;
            this.MinZoom = 1;
            this.Zoom = 1;
            this.DragButton = System.Windows.Forms.MouseButtons.None;
            
            //this.DisableAltForSelection = true; 
            m_MapOverlay = new GMapOverlay("EyeOverlay");
            this.Overlays.Add(m_MapOverlay);
             
        }
         
        /// <summary>
        /// 设置当前显示的矩形框
        /// </summary>
        /// <param name="lstrect"></param>
        public void SetRect(List<PointLatLng> lstrect)
        {
            if (m_EyeRect == null)
            {
                m_EyeRect = new GMapPolygon(lstrect, "EYE_RECT");
                m_EyeRect.IsHitTestVisible = true;
                m_EyeRect.Stroke.Width = 2;
                m_EyeRect.Stroke.Color = Color.Blue;  
                m_MapOverlay.Polygons.Add(m_EyeRect);
            }
            else
            {
                m_EyeRect.Stroke.Color = Color.Blue;  
                m_EyeRect.Points.Clear();
                if (lstrect != null && lstrect.Count > 0)
                    m_EyeRect.Points.AddRange(lstrect);
            }

            base.Refresh();
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            Position = FromLocalToLatLng(e.X, e.Y);

        }
    }
}
