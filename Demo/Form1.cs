﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace GmapEn.Demo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            m_ClickTimer = new System.Windows.Forms.Timer();
            m_ClickTimer.Tick += new EventHandler(m_ClickTimer_Tick);
            m_ClickTimer.Interval = 80;
        }

        void m_ClickTimer_Tick(object sender, EventArgs e)
        { 
            if (!m_bThdRunning)
            {
                m_nGlobalCounter++;
                m_bThdRunning = true;
                FrmDialog dlg = new FrmDialog();
                dlg.OnNotify += new EventHandler(dlg_OnNotify);
                if(dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                { 
                    if (m_Thd != null)
                    {
                        //m_Thd.DisableComObjectEagerCleanup();
                    }
                    m_Thd = new Thread(new ThreadStart(Test));
                    m_Thd.IsBackground = true;
                    m_Thd.Start();
                }
            }
        }

        void dlg_OnNotify(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

        }
         
        System.Windows.Forms.Timer m_ClickTimer;
        bool m_bThdRunning = false;
        int m_nGlobalCounter = 0;
        Thread m_Thd;
        private void Start_Click(object sender, EventArgs e)
        {
            m_ClickTimer.Start();
        }
        void Test()
        {
            this.Invoke(new Action<int>((i) =>
            {
                label1.Text = "调用:" + i;
            }), m_nGlobalCounter); 
            m_bThdRunning = false;
        }

        private void Stop(object sender, EventArgs e)
        {
            m_ClickTimer.Stop();
        }
    }
}
